##
#
# A straightforward pipeline that build a reference and analyse paired end reads with kaiju, based on NCBI taxonomy.
#
# https://lemmi.ezlab.org
# http://kaiju.binf.ku.dk/
#
# 1) Organise your reference and reads following the LEMMI guideline: https://gitlab.com/ezlab/lemmi/wikis/user-guide
#
# 2) snakemake -r --config uid=your_user_uid
# # To get your user id on unix env, the command is `id`. Don't use your username, and avoid using root.
#
# Defautl is 4G of memory, so won't get far with this, and one CPU. Please edit to suit your environment.
#
rule all:
    """
    expected outputs
    """
    input:
        ref1 = "bbx/reference/built.bwt",
        ref2 = "bbx/reference/built.fmi",
        ref3 = "bbx/reference/built.sa",
        bins = "bbx/output/bins.species.tsv",
        profile = "bbx/output/profile.species.tsv"
rule build_ref:
    """
    build a reference compatible with kaiju using all faa file mentionned in the mapping.tsv
    """
    input:
        mapping = "bbx/input/training/mapping.tsv"
    output:
        ref1 = "bbx/reference/built.bwt",
        ref2 = "bbx/reference/built.fmi",
        ref3 = "bbx/reference/built.sa",
    shell:
        """
        rm bbx/tmp/* || true
        echo "Building the docker image... output sent to docker_build.log"
        docker build . -t lemmi_kaiju > docker_build.log
        echo "Docker build done"
        docker run -u {config[uid]} --name lemmi_kaiju_build -v "$(pwd)/bbx/tmp:/bbx/tmp:rw" -v "$(pwd)/bbx/input/:/bbx/input/:ro" -v "$(pwd)/bbx/reference/:/bbx/reference/:rw" -v "$(pwd)/bbx/output:/bbx/output:rw" -v "$(pwd)/bbx/metadata:/bbx/metadata:rw" -e "taxlevel=species" -e "reference=built" -e "thread=1" --cpus 1 -m "4G" lemmi_kaiju build_ref || true
        docker rm lemmi_kaiju_build
        mv bbx/metadata/log.txt bbx/metadata/log_build.txt
        """
rule analysis:
    """
    analysis of paired end reads to return a profile and bins
    """
    input:
        ref1 = "bbx/reference/built.bwt",
        ref2 = "bbx/reference/built.fmi",
        ref3 = "bbx/reference/built.sa",
        r1 = "bbx/input/testing/reads.1.fq.gz",
        r2 = "bbx/input/testing/reads.2.fq.gz"
    output:
        bins = "bbx/output/bins.species.tsv",
        profile = "bbx/output/profile.species.tsv"
    shell:
        """
        rm bbx/tmp/* || true
        docker run -u {config[uid]} --name lemmi_kaiju_analysis -v "$(pwd)/bbx/tmp:/bbx/tmp:rw" -v "$(pwd)/bbx/input/:/bbx/input/:ro" -v "$(pwd)/bbx/reference/:/bbx/reference/:ro" -v "$(pwd)/bbx/output:/bbx/output:rw" -v "$(pwd)/bbx/metadata:/bbx/metadata:rw" -e "taxlevel=species" -e "reference=built" -e "thread=1" --cpus 1 -m "4G" lemmi_kaiju analysis || true
        docker rm lemmi_kaiju_analysis
        """
