outp = open('/bbx/output/profile.tsv', 'w')
# load taxonomy
taxo = {}
for line in open('/bbx/tmp/taxo_mapping.tsv'):
    taxo.update({line.strip().split('\t')[0]:line.strip().split('\t')[1]})
outp.write('# Taxonomic Profiling Output\n@SampleID:kaiju\n@Version:0.9.1\n@Ranks:superkingdom|phylum|class|order|family|genus|species\n@@TAXID\tRANK\tTAXPATH\tTAXPATHSN\tPERCENTAGE\n')
rank = ''
for line in open('/bbx/tmp/abundance'):
    if '%' in line:
        rank = line.strip().split('reads')[-1].replace(' ','').split('\t')[-1]
    else:
        if not line.startswith('-') and not 'cannot' in line and not 'unclassified' in line:
            outp.write('%s\t%s\t%s\t%s\t%s\n' % (taxo[line.strip().split('\t')[-1]], rank, taxo[line.strip().split('\t')[-1]], taxo[line.strip().split('\t')[-1]], line.strip().split('\t')[0].replace(' ','')))
outp.close()
outp=open('/bbx/output/bins.tsv','w')
outp.write('# Taxonomic Binning Output\n')
outp.write('@SampleID:kaiju\n')
outp.write('@Version:0.9.0\n')
outp.write('@Ranks:superkingdom|phylum|class|order|family|genus|species\n')
outp.write('@@SEQUENCEID\tBINID\n')
for line in open('/bbx/tmp/kaiju.out'):
    if line.startswith('C'):
        outp.write('%s\t%s\n' % (line.strip().split()[1],line.strip().split()[2]))
outp.close()
