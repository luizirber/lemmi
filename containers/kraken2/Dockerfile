# Dockerfile for building a benchmarking biobox
# *** where you need to make changes

FROM ubuntu:latest

####
#
# 1) install the common required stuff here
#

ENV PACKAGES make gcc wget libc6-dev zlib1g-dev ca-certificates xz-utils
RUN apt-get update --fix-missing -y && apt-get install -y --no-install-recommends ${PACKAGES}
RUN apt-get install --fix-missing python3 git build-essential -y
RUN yes | perl -MCPAN -e "CPAN::Shell->notest(qw!install  List::MoreUtils!)"
RUN yes | perl -MCPAN -e "CPAN::Shell->notest(qw!install  Parallel::ForkManager!)"

####
#
# 2) Biobox file validator
#

ENV VALIDATOR /bbx/validator/
ENV BASE_URL https://s3-us-west-1.amazonaws.com/bioboxes-tools/validate-biobox-file
ENV VERSION  0.x.y
RUN mkdir -p ${VALIDATOR}

# download the validate-biobox-file binary and extract it to the directory $VALIDATOR
RUN wget \
      --quiet \
      --output-document -\
      ${BASE_URL}/${VERSION}/validate-biobox-file.tar.xz \
    | tar xJf - \
      --directory ${VALIDATOR} \
      --strip-components=1

ENV CONVERT https://github.com/bronze1man/yaml2json/raw/master/builds/linux_386/yaml2json
# download yaml2json and make it executable
#RUN cd /usr/local/bin && wget --quiet ${CONVERT} && chmod 700 yaml2json

ENV JQ http://stedolan.github.io/jq/download/linux64/jq
# download jq and make it executable
RUN cd /usr/local/bin && wget --quiet ${JQ} && chmod 700 jq

# chmod so it can be run by non root
RUN chmod go+x /usr/local/bin/jq
#RUN chmod go+x /usr/local/bin/yaml2json

ADD yaml2json /usr/local/bin

####
#
# ***3) HERE INSTALL YOUR SOFTWARE.
#
# e.g.

ENV KRAKEN2_DIR /kraken2

RUN git clone https://github.com/DerrickWood/kraken2.git $KRAKEN2_DIR 

WORKDIR $KRAKEN2_DIR

RUN git checkout 3167304affcff626db886c42c0ad3011965697a6

RUN ./install_kraken2.sh $KRAKEN2_DIR

RUN apt-get install ncbi-blast+ -y

ENV PATH ${PATH}:${VALIDATOR}:$KRAKEN2_DIR/scripts:$KRAKEN2_DIR:$KRAKEN2_DIR/bin

####
#
# ***4) ADD the additional files, i.e. Taskfile, entry, and any other you need
#

# You have to edit this one:
ADD Taskfile /

# For Kraken2, these custom ones were added
# don't forget to fix the mod before, i.e. chmod +x, if you want to call ./scriptname
ADD bash/prepare_inputs.sh /
ADD bash/gunzip.sh /
ADD python/prepare_inputs.py /
ADD bash/add_to_library.sh /
ADD bash/add_and_rm.sh /
ADD bash/edit_fasta.sh /
ADD python/prepare_results.py /

# Don't touch this one:
ADD schema.yaml /
# Add assemble script to the directory /usr/local/bin inside the container.
# /usr/local/bin is appended to the $PATH variable what means that every script
# in that directory will be executed in the shell without providing the path.
# Don't change the location of this, don't edit this file
ADD entry /usr/local/bin/

####
#
# ***5) Create the user that will be called by the pipeline instead of root (one day, benchuser instead of my personal one): don't change if you build for submission, but for your test (it has to be an unpriviledged user)
#

RUN useradd -m seppey

####
#
# 6) Define the entrypoint, i.e. the file that is called when the container is launched
#
# Don't touch this
#
ENTRYPOINT ["entry"]
