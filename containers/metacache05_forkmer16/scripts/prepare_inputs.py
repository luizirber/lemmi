#
# Parse the mapping file and edit the fasta header for metacache

import os
import re

acc2taxid = open('/bbx/tmp/acc2taxid', 'w')
acc2taxid.write('accession\taccession.version\ttaxid\tgi\n')
for line in open('/bbx/input/training/mapping.tsv'):
    if line.startswith('#') or line.startswith('@'):
        continue
    name = line.strip().split('\t')[4]
    taxid = line.strip().split('\t')[0]
    inp = '/bbx/tmp/%s.fna' % name
    outp = open('/bbx/tmp/%s.fna.edited' % name, 'w')
    for line2 in open(inp):
        if line2.startswith('>'):
            acc = line2.split(" ")[0][1:].strip("\n") # take only accession , adds \t as separator
            acc2taxid.write('%s\t1\t%s\t%s\n' % (acc, taxid, acc))
            outp.write('>%s|%s\n' % (acc, acc))
        else:
            outp.write(line2)
    os.remove(inp) # save disk space

outp.close()
