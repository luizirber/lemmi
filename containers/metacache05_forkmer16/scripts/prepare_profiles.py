#

import csv

data=csv.reader(open('/bbx/tmp/mappings.txt', 'r'), delimiter='\t')
mappings = {k: v for k, v in data}
outp=open('/bbx/output/profile.tsv','w')
ranks_header = open('/bbx/input/training/mapping.tsv').readlines()[2]
outp.write('# Taxonomic Profiling Output\n')
outp.write('@SampleID:metacache\n')
outp.write('@Version:0.9.1\n')
outp.write(ranks_header)
outp.write('@@TAXID\tRANK\tTAXPATH\tTAXPATHSN\tPERCENTAGE\n') # !!! TAXPATHSN\t

for line in open('/bbx/tmp/abundances_clean.txt'):
    if line.startswith('#') or line.startswith('None'):
        continue
    taxa=line.split('\t')[2]
    taxid=mappings.get(taxa)
    if taxid == None:
        continue
    percentage=line.split('\t')[6].replace('%', '').strip("\n")
    rank=line.split('\t')[0]
    outp.write('%s\t%s\t%s\t%s\t%s\n' % (taxid, rank, taxid, taxid, percentage))
