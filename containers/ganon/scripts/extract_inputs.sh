#!/bin/bash
thread=$1
find /bbx/input/training/ -name "*.f?a.gz" -print0 | xargs -0 -I {} -P $thread /gunzip.sh {}
